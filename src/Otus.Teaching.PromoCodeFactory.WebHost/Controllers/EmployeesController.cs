﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepositorym, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepositorym;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = await GetEmployeeResponseAsync(employee);
            //     new EmployeeResponse()
            // {
            //     Id = employee.Id,
            //     Email = employee.Email,
            //     Roles = employee.Roles.Select(x => new RoleItemResponse()
            //     {
            //         Name = x.Name,
            //         Description = x.Description
            //     }).ToList(),
            //     FullName = employee.FullName,
            //     AppliedPromocodesCount = employee.AppliedPromocodesCount
            // };

            return employeeModel;
        }

        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(Employee employee)
        {
            var employeeDd = await _employeeRepository.GetByIdAsync(employee.Id);

            if (employeeDd != null)
            {
                return Problem("item exist!");
            }

            if (employee.Roles != null)
            {
                List<Role> rolesDb = new List<Role>();
                
                foreach (var role in employee.Roles)
                {
                    var roleDb = await _roleRepository.GetByIdAsync(role.Id);
                    
                    if (roleDb == null)
                        return Problem($"Role with id = {role.Id} does not exist");
                    else
                        rolesDb.Add(roleDb);
                }

                employee.Roles = rolesDb;
            }
            
            employeeDd = await _employeeRepository.AddAsync(employee);

            return await GetEmployeeResponseAsync(employeeDd);
        }

        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(Employee employee)
        {
            var employeeDb= await _employeeRepository.GetByIdAsync(employee.Id);

            if (employeeDb == null)
            {
                return NotFound();
            }
            
            if (employee.Roles != null)
            {
                List<Role> rolesDb = new List<Role>();
                
                foreach (var role in employee.Roles)
                {
                    var roleDb = await _roleRepository.GetByIdAsync(role.Id);
                    
                    if (roleDb == null)
                        return Problem($"Role with id = {role.Id} does not exist");
                    else
                        rolesDb.Add(roleDb);
                }

                employee.Roles = rolesDb;
            }
            
            employeeDb = await _employeeRepository.UpdateAsync(employee);

            return await GetEmployeeResponseAsync(employeeDb);
        }
        
        [HttpDelete]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployeeByIdAsync(Guid id)
        {
            var employeeDb= await _employeeRepository.GetByIdAsync(id);

            if (employeeDb == null)
            {
                return NotFound();
            }
            
            employeeDb = await _employeeRepository.DeleteByIdAsync(id);

            return await GetEmployeeResponseAsync(employeeDb);
        }
        private async Task<EmployeeResponse> GetEmployeeResponseAsync(Employee employee)
        {
            EmployeeResponse employeeModel = null;
            
            if (employee != null)
            {
                employeeModel = new EmployeeResponse()
                {
                    Id = employee.Id,
                    Email = employee.Email,
                    Roles = employee.Roles?.Select(x => new RoleItemResponse()
                    {
                        Name = x.Name,
                        Description = x.Description
                    }).ToList(),
                    FullName = employee.FullName,
                    AppliedPromocodesCount = employee.AppliedPromocodesCount
                };
            }

            return await Task.FromResult(employeeModel);
        }
    }
}