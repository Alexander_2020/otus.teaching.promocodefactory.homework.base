﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected static IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data ??= data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T item)
        {
            Data = Data.Append(item);
            return Task.FromResult(item);
        }

        public Task<T> UpdateAsync(T item)
        {
            var itemDb = Data.FirstOrDefault(x => x.Id == item.Id);

            if (itemDb != null)
            {
                var properties = typeof(T).GetProperties();

                foreach (var property in properties)
                {
                    if(property.CanWrite)
                        property.SetValue(itemDb, property.GetValue(item));
                }
            }

            return Task.FromResult(itemDb);
        }

        public Task<T> DeleteByIdAsync(Guid id)
        {
            var itemDb = Data.FirstOrDefault(x => x.Id == id);

            if (itemDb != null)
            {
                Data = Data.Where(x => x.Id != id);
            }

            return Task.FromResult(itemDb);
        }
    }
}